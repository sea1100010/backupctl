#!/bin/python3
# https://gitlab.com/sea1100010/backupctl

import argparse
import os
import csv
import sys
from shutil import make_archive
from datetime import datetime
from glob import glob

def backup():
    result = "success"
    parser = argparse.ArgumentParser(prog='backupctl',description='convenient backup tool')
    parser.add_argument('--directory', required=True, help='directory for backup (required option)')
    parser.add_argument('--output', help='path for backup')
    parser.add_argument('-a', dest='type', default='gztar', help='type of compressing (default: %(default)s)')
    parser.add_argument('-j', dest='logfile', default='journal.csv', help='a name of logfile (default: %(default)s)')
    args = parser.parse_args()

    if args.output == None:
        args.output = os.getcwd()

    if not os.path.exists(args.directory):
        os.write(2, bytes(args.directory+" is not exist\n",'utf-8'))
        result = "fail"

    if args.type not in {'zip', 'tar', 'gztar', 'bztar', 'xztar'}:
        os.write(2, bytes(args.type + " is not correct.  Choose one of zip, tar, gztar, bztar, xztar\n",'utf-8'))
        result = "fail"

    archive_name = ''
    if result == "success":
        archive_name=str(os.path.basename(args.directory))
        archive_name=archive_name+'_'+datetime.utcnow().strftime("%Y-%m-%d_%H:%M:%S")
        archive_name=os.path.join(args.output,archive_name)
        try:
            make_archive(archive_name,args.type,args.directory)
        except OSError:
            e = sys.exc_info()[1]
            os.write(2, bytes("Error creating file " + archive_name + ". " + os.strerror(e.args[0]) + "\n", 'utf-8'))
            result = "fail"
        else:
            archive_name = glob(archive_name + ".*")[0]
            os.write(1, bytes("Created " + archive_name + "\n", 'utf-8'))

    try:
        f = open(args.logfile, 'a', newline='')
    except OSError:
        e = sys.exc_info()[1]
        os.write(2, bytes("Error creating logfile " + args.logfile+". " + os.strerror(e.args[0]) + "\n", 'utf-8'))
    else:
        w = csv.writer(f, dialect='excel')
        w.writerow([datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), args.directory, archive_name, result])
        f.close()

    return 0

if __name__ == '__main__':
    backup()

